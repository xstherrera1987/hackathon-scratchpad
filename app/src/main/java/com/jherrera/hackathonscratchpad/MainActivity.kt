package com.jherrera.hackathonscratchpad

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.jherrera.hackathonscratchpad.databinding.CommentBinding
import java.util.*

class MainActivity : AppCompatActivity() {

  val commentsListView: RecyclerView by lazy { findViewById<RecyclerView>(R.id.commentsList) }
  val commentsList = listOf(
    CommentViewModel(
      "other kind of comment",
      9,
      Date(),
      "RF-Caliber",
      "Brooklyn, NY",
      195
    )
  )

  var binding : CommentBinding? = null
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

//    setContentView(R.layout.comment)
    binding = DataBindingUtil.setContentView<CommentBinding>(this, R.layout.comment).also {
      it.viewModel = CommentViewModel(
        "some kind of comment",
        3,
        Date(),
        "RF-Calibre",
        "London, UK",
        196
      )
    }
  }

  override fun onResume() {
    super.onResume()

//    commentsListView.adapter = PartsSharingRVAdapter(this, commentsList)
  }
}


class PartsSharingRVAdapter(context: Context, val comments: List<CommentViewModel>) :
  RecyclerView.Adapter<PartsSharingVH>() {
  val layoutInflater = LayoutInflater.from(context)

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PartsSharingVH {
    val rootView = layoutInflater.inflate(R.layout.comment, parent, false)
    return PartsSharingVH(rootView as ViewGroup)
  }

  override fun getItemCount(): Int {
    return comments.size
  }

  override fun onBindViewHolder(holder: PartsSharingVH, position: Int) {
    val vm = comments[position]
    holder.bindViewModel(vm)
  }
}

class PartsSharingVH(val rootView: ViewGroup) : ViewHolderBase<CommentViewModel>(rootView) {
  var binding: CommentBinding? = null

  override fun bindViewModel(viewModel: CommentViewModel) {
    binding = DataBindingUtil.bind<CommentBinding>(rootView)?.also {
      it.viewModel = viewModel
    }
  }
}
