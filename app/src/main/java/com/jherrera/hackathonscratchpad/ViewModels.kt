package com.jherrera.hackathonscratchpad

import java.text.SimpleDateFormat
import java.util.*

data class PartSharingViewModel(
  val partDescription: String = "",
  val partNumber: String = ""
) : BindableViewModel() {

  var comment: String? = null
  var photoGuid: String? = null

  companion object {

    fun create(partItemModel: IPartLineItemViewModel): PartSharingViewModel {

      return PartSharingViewModel(
        partItemModel.partDescriptionText(),
        partItemModel.partNumberText()
      )
    }
  }
}

val dateFormatter = SimpleDateFormat("MM/dd/yyyy", Locale.US)
class CommentViewModel(
  val commentText: String,
  val numLikes: Int,
  val postedDate: Date,
  val repairFacilityName: String,
  val repairFacilityLocation: String,
  val repairFacilityReputation: Int
) {
  fun repairFacilityReputationText() = repairFacilityReputation.toString()
  fun numLikesText() = numLikes.toString()
  fun postedDateText() = dateFormatter.format(postedDate)
}

interface IPartLineItemViewModel : PartsItemsModel {
  fun partNumberText(): String
  fun lineNumberText(): String
  fun partDescriptionText(): String
  fun dueDateText(): String
  fun roQuantityText(): String
  fun orderedQuantityText(): String
  fun receivedQuantityText(): String
  fun statusText(): String
  fun commentCount(): String
  fun commentCountVisible(): Boolean
  fun openComments()
}

/** common interface for parts item/header/footer view-models */
interface PartsItemsModel
