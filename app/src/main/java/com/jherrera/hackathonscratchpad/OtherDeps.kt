package com.jherrera.hackathonscratchpad


import android.os.Parcelable
import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.databinding.BaseObservable
import androidx.databinding.Observable
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView


abstract class ViewHolderBase<TViewModel>(itemView: View) : RecyclerView.ViewHolder(itemView) {
  abstract fun bindViewModel(viewModel: TViewModel)
}

interface ViewTemplateSelector {
  fun getViewForViewType(viewModelType: Int, parent: ViewGroup): View
}

class ViewModelDiffCallback<T : ViewModel> : DiffUtil.ItemCallback<T>() {
  override fun areItemsTheSame(oldItem: T, newItem: T): Boolean {
    return false
  }

  override fun areContentsTheSame(oldItem: T, newItem: T): Boolean {
    return false
  }
}

interface ParcelableObservable : Observable, Parcelable


/** view-model for android data-binding */
interface IBindableViewModel : Observable

/** base view-model for data-binding */
open class BindableViewModel : BaseObservable(), IBindableViewModel

fun log(msg: String) {
  Log.w("Hackathon", msg)
}